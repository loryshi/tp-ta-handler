package com.bill99.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import bill99qa.interfaces.schema.ino.Message;

public class TestXmlParse {

	public static String parse(String xml) {
		String aString = "";
		try {
			JAXBContext jc = JAXBContext.newInstance("bill99qa.interfaces.schema.ino");
			//ObjectFactory objFactory = new ObjectFactory(); //生成对象工厂
			Unmarshaller um = jc.createUnmarshaller();
			//			Message message = (Message) um.unmarshal(new StringReader(xml));
			Message message = (Message) um.unmarshal(new File("D://TestXmlParse.xml"));

			//MsgContent stu=(MsgContent) um.unmarshal(new StringReader(xml));

			aString = message.getMsgContent().getIdTxn();

			//aString = message.getResponseCode();

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //参数为JAXB生成的java文件所在包名
		return aString;
	}

	public static void main(String[] args) {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><message><version>1.0</version><appVersion>MP_AND_APP_S2_99BILL_02_01_13012315_01</appVersion><bizType>M012</bizType><merchantId>999999945110001</merchantId><responseCode>00</responseCode><responseMsg>交易成功</responseMsg><deviceInfo><mac>50:CC:F8:D9:8F:EE</mac><imei>353328056752247</imei><imsi>460012130504870</imsi></deviceInfo><msgContent><orderId>20130528114400</orderId><productName>大风车</productName><idTxn>000008803015</idTxn><authCode>644535</authCode><cardOrg>CU</cardOrg><issuer>交通银行</issuer><txnTime>20130528114403</txnTime><merchantName>快刷自动化测试商户</merchantName><stlMerchantId>999999945110001</stlMerchantId><stlMerchantName>快刷自动化测试商户</stlMerchantName></msgContent></message>";
		String bString = TestXmlParse.parse(xml);
		System.out.println("===" + bString);
	}

}
