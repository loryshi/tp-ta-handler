package com.bill99.test;

public class Test {

	public static void main(String[] args) {
		final String[] numSet = { "有效期", "CVV2", "姓名", "身份证", "手机号" };
		long max = 1 << numSet.length;
		for (int i = 1; i < max; i++) {
			for (int j = 0; j < numSet.length; j++) {
				if ((i & (1 << j)) != 0) {
					System.out.print("卡号, " + numSet[j] + ", ");
				}
			}
			System.out.println();
		}

	}

}
