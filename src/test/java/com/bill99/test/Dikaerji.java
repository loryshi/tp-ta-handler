package com.bill99.test;

import java.util.ArrayList;
import java.util.List;

public class Dikaerji {
	public static void main(String[] args) {
		String[] str1 = { "1-", "2-", "3-" };
		String[] str2 = { "2", "3", "4" };
		// String[] str3 = { "一", "二", "三" };
		// String[] str4 = { "4", "5", "6" };
		// String[] str5 = { "7", "8" };
		// String[] str6 = { "9", "+", "-" };
		List<String[]> list = new ArrayList<String[]>();
		list.add(str1);
		list.add(str2);
		// list.add(str3);
		// list.add(str4);
		// list.add(str5);
		// list.add(str6);
		List<String> result = new ArrayList<String>();
		Descartes(list, 0, result, "");
		for (int i = 0; i < result.size(); i++) {
			System.out.println(result.get(i));
		}
	}

	private static String Descartes(List<String[]> list, int count, List<String> result, String data) {
		String temp = data;
		// 获取当前数组
		String[] astr = list.get(count);
		// 循环当前数组
		// foreach (var item in astr)
		for (int i = 0; i < astr.length; i++) {
			if (count + 1 < list.size()) {
				temp += Descartes(list, count + 1, result, data + astr[i]);
			} else {
				result.add(data + astr[i]);
			}
		}
		return temp;
	}
}
