package com.bill99.test;

import java.util.ArrayList;
import java.util.List;

//~ Package Declaration
//============================================================================

//~ Comments
//============================================================================

public class CrossUtils {

	// ~ Static Fields
	// ==========================================================================

	// ~ Fields
	// ==========================================================================

	// ~ Constructors
	// ==========================================================================

	// ~ Methods
	// ==========================================================================

	public static List<List<String>> cross(List<List<String>> crossArgs) {

		// 计算出笛卡尔积行数
		int rows = crossArgs.size() > 0 ? 1 : 0;

		for (List<String> data : crossArgs) {
			rows *= data.size();
		}

		// 笛卡尔积索引记录
		int[] record = new int[crossArgs.size()];

		List<List<String>> results = new ArrayList<List<String>>();

		// 产生笛卡尔积
		for (int i = 0; i < rows; i++) {
			List<String> row = new ArrayList<String>();

			// 生成笛卡尔积的每组数据
			for (int index = 0; index < record.length; index++) {
				row.add(crossArgs.get(index).get(record[index]));
			}

			results.add(row);
			crossRecord(crossArgs, record, crossArgs.size() - 1);
		}

		return results;
	}

	private static void crossRecord(List<List<String>> sourceArgs, int[] record, int level) {
		record[level] = record[level] + 1;

		if (record[level] >= sourceArgs.get(level).size() && level > 0) {
			record[level] = 0;
			crossRecord(sourceArgs, record, level - 1);
		}
	}

	public static void main(String[] args) {

		List<List<String>> crossArgs = new ArrayList<List<String>>();

		List<String> main = new ArrayList<String>();
		main.add("卡号");

		List<String> sub = new ArrayList<String>();
		sub.add("有效期");
		sub.add("CVV2");
		sub.add("姓名");
		sub.add("身份证");
		sub.add("手机号");

		crossArgs.add(main);
		crossArgs.add(sub);

		List<List<String>> resultList = cross(crossArgs);
		System.out.println(resultList.size());

		for (List<String> list : resultList) {

			System.out.println(list.size());
			for (String string : list) {
				System.out.println(string);
			}

		}
	}
}
