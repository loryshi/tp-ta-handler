package com.bill99.testmp.testmanage.orm.manager;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.entity.TaBuild;

@ContextConfiguration(locations = { "classpath*:context/applicationContext.xml" })
public class TaBuildMngTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TaBuildMng taBuildMng;

	@Test
	public void testQuery4rp() {
	}

	@Test
	public void testSave() {
	}

	@Test
	public void testUpdate() {
	}

	@Test
	public void testQueryByJob() {
	}

	@Test
	public void testMerge() {
		TaBuild taBuild = new TaBuild();
		taBuild.setJobName("leotest");
		taBuild.setBuildNum("1");
		taBuild.setBuildId("3");
		//		taBuild.setTaBuildPk(taBuildPk);
		//		taBuildMng.save(taBuild);
		taBuildMng.merge(taBuild);
	}

}
