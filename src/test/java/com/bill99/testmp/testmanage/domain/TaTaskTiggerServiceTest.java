package com.bill99.testmp.testmanage.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.ta.mdp.api.common.TaTaskRequestBo;
import com.bill99.testmp.ta.mdp.api.common.UnitUtil;

@ContextConfiguration(locations = { "classpath*:test/context/applicationContext.xml" })
public class TaTaskTiggerServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TaTaskTiggerService taTaskTiggerService;

	@Test
	public void testCheckTaskTigger() {
		taTaskTiggerService.checkTaskTigger();
	}

	@Test
	public void testTiggerTask() {

		TaTaskRequestBo taTaskRequestBo = new TaTaskRequestBo();
		taTaskRequestBo.setStageId(UnitUtil.STAGEID_STAGE2);
		taTaskRequestBo.setGroupId(UnitUtil.GROUPID_T);

		taTaskTiggerService.tiggerTask(1L, taTaskRequestBo);
	}

	@Test
	public void testAutoTiggerTask() {
		taTaskTiggerService.autoTiggerTask();
	}

	@Test
	public void testAutoTiggerTask4Regression() {
		taTaskTiggerService.autoTiggerTask4Regression();
	}

}
