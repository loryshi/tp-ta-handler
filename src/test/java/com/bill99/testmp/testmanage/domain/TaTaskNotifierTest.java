package com.bill99.testmp.testmanage.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.bill99.testmp.testmanage.orm.manager.TaMng;

@ContextConfiguration(locations = { "classpath*:test/context/applicationContext.xml" })
public class TaTaskNotifierTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TaTaskNotifier taTaskNotifier;
	@Autowired
	private TaMng taMng;

	@Test
	public void testTaTaskEmailReport() {
		//		taTaskNotifier.taTaskEmailReport("TEST_Ex2-1000000-012", 1365661994080L, 63L);
		
//		taTaskNotifier.taTaskEmailReport("MA_FI-129-01", 1363332236428L, 65L);

		taTaskNotifier.taTaskEmailReport("MA_CPS-1000254-1", 1369669879642l, 62L);

		//		double a=taMng.getTcStatusCounts("TEST_Ex2-1000000-012", 1365661994080L,"1");
		//		System.out.println(a);
	}

}
