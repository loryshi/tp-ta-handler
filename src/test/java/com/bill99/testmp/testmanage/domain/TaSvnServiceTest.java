package com.bill99.testmp.testmanage.domain;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath*:test/context/applicationContext.xml" })
public class TaSvnServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TaSvnService taSvnService;

	@Test
	public void testInitTaSvnPath() {
		taSvnService.initTaSvnPath();
	}

}
