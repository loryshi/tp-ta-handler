//package com.bill99.testmp.testmanage.common.schema;
//
//import java.io.FileOutputStream;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Marshaller;
//
//import org.junit.Test;
//
//import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
//
//public class ObjectFactoryTest {
//
//	@Test
//	public void test() {
//		try {
//
//			JAXBContext jc = JAXBContext.newInstance("com.bill99.testmp.testmanage.common.schema"); //参数为JAXB生成的java文件所在包名
//			ObjectFactory objFactory = new ObjectFactory(); //生成对象工厂
//
//			List<Include> includes = new ArrayList<Include>();
//
//			Include include = new Include();//Include
//			include.setName("push");
//			includes.add(include);
//
//			Methods method = new Methods();//Methods
//			method.setInclude(includes);
//
//			List<Methods> methods = new ArrayList<Methods>();
//			methods.add(method);
//
//			List<Class> clazzs = new ArrayList<Class>();
//			Class clazz = new Class();
//			clazz.setName("com.bill99.ta.demo.LeoTest");
//			clazz.setMethods(methods);
//			clazzs.add(clazz);
//
//			Classes classe = new Classes();
//			classe.setClazz(clazzs);
//
//			com.bill99.testmp.testmanage.common.schema.Test test = new com.bill99.testmp.testmanage.common.schema.Test();
//			test.setClasses(classe);
//			test.setName("leoteset");
//
//			Suite suite = objFactory.createSuite();
//			suite.setName("leotest");
//			ArrayList<com.bill99.testmp.testmanage.common.schema.Test> tests = new ArrayList<com.bill99.testmp.testmanage.common.schema.Test>();
//			tests.add(test);
//			suite.setTest(tests);
//
//			Marshaller marshaller = jc.createMarshaller();
//			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//			marshaller.marshal(suite, new FileOutputStream("src/test/resources/test/context/schema/test.xml"));
//
//			OutputStream os = new ByteOutputStream();
//			marshaller.marshal(suite, os);
//			System.out.println(os.toString());
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
