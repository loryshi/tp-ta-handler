package com.bill99.riaframework.domain.mail;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;

public interface EmailSender {

	/**
	 * 发送email
	 * @param recvAddress 收件人地址
	 * @param subject 主题
	 * @param content 内容
	 * @param fromAddress 发件人
	 * @param bytesList 附件文件内容
	 * @param nameList 附件文件名列表
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	public void sendEmail(List<String> recvAddress, String subject, String content, String fromAddress, List bytesList, List nameList) throws UnsupportedEncodingException,
			MessagingException;

	/**
	 * 发送带附件的邮件
	 * @param recvAddress 收件人地址
	 * @param ccAddress 抄送人地址
	 * @param subject 主题
	 * @param content 内容
	 * @param fromAddress 发件人
	 * @param bytesList 附件文件内容
	 * @param nameList 附件文件名列表
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, String subject, String content, String fromAddress, List bytesList, List nameList)
			throws UnsupportedEncodingException, MessagingException;

	public void sendEmail(List<String> recvAddress, String subject, String content, String fromAddress) throws Exception;

	/**
	 * 发送邮件 附带抄送人/密送
	 * @param recvAddress 收件人地址
	 * @param ccAddress 抄送人地址
	 * @param subject 主题
	 * @param content 内容
	 * @param fromAddress 发件人
	 * @throws Exception
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, String subject, String content, String fromAddress) throws Exception;

	/**
	 * 
	 * @param recvAddress 收件人地址
	 * @param ccAddress 抄送人地址
	 * @param bccAddress 密送人地址
	 * @param subject 主题
	 * @param content 内容
	 * @param fromAddress 发件人
	 * @param bytesList 附件文件内容 (无附件时传null即可)
	 * @param nameList 附件文件名列表
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	public void sendEmail(List<String> recvAddress, List<String> ccAddress, List<String> bccAddress, String subject, String content, String fromAddress, List bytesList,
			List nameList) throws UnsupportedEncodingException, MessagingException;

}