package com.bill99.riaframework.common.dto;

public class NotifyTaResultClassMethodDto {

	private String taClassName;
	private String packageName;

	public String getTaClassName() {
		return taClassName;
	}

	public void setTaClassName(String taClassName) {
		this.taClassName = taClassName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

}
