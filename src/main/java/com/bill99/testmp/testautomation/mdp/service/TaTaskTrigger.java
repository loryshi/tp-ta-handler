package com.bill99.testmp.testautomation.mdp.service;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.ta.mdp.api.common.TaTaskRequestBo;
import com.bill99.testmp.ta.mdp.api.common.TaTaskResponesBo;
import com.bill99.testmp.ta.mdp.api.service.TaTaskService;
import com.bill99.testmp.testmanage.domain.TaTaskTiggerService;

public class TaTaskTrigger implements TaTaskService {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaTaskTiggerService taTaskTiggerService;

	public TaTaskResponesBo triggerTaTask(TaTaskRequestBo taTaskRequestBo) {

		logger.info("@@@==>		triggerTaTask trigger		<==@@@");

		return taTaskTiggerService.triggerTaTask(taTaskRequestBo);

	}

	public void setTaTaskTiggerService(TaTaskTiggerService taTaskTiggerService) {
		this.taTaskTiggerService = taTaskTiggerService;
	}

	public void autoTiggerTask() {
		taTaskTiggerService.autoTiggerTask();
	}

}
