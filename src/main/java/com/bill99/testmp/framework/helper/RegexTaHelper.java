package com.bill99.testmp.framework.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class RegexTaHelper {

	private final static String regexTestMethodPackage = "package(?:\\s+).*?(?:\\s*);";
	private final static Pattern patternTestMethodPackage = Pattern.compile(regexTestMethodPackage);

	public static String getTestMethodPackage(String classContenx) {
		String packageName = "";
		Matcher matcher = patternTestMethodPackage.matcher(classContenx);
		while (matcher.find()) {
			packageName = classContenx.substring(matcher.start(), matcher.end());
			packageName = packageName.replace("package", "").replace(";", "").trim();
		}
		return packageName;
	}

	private final static String regexTestMethodDescription = "description(\\s*)=(\\s*)\\\".*?\"";
	private final static Pattern patternTestMethodDescription = Pattern.compile(regexTestMethodDescription);

	public static String getTestMethodDescription(String classContenx) {
		String description = "";
		Matcher mDescription = patternTestMethodDescription.matcher(classContenx);
		while (mDescription.find()) {
			description = classContenx.substring(mDescription.start(), mDescription.end());
			description = description.replace("description", "").replace("=", "").replace("\"", "").trim();
		}
		return description;
	}

	private final static String regexDependsOnMethods = "dependsOnMethods(\\s*)=(\\s*)[{|?\"].*[}|?\"]";
	private final static Pattern patternDependsOnMethods = Pattern.compile(regexDependsOnMethods);

	public static String getDependsOnMethods(String classContenx) {
		String description = "";
		Matcher mDescription = patternDependsOnMethods.matcher(classContenx);
		while (mDescription.find()) {
			description = classContenx.substring(mDescription.start(), mDescription.end());
			description = description.replace("dependsOnMethods", "").replace("=", "").replace("\"", "").replace("{", "").replace("}", "").trim();
		}
		return description;
	}

	private final static String getTestCaseIdRegex = "(.*?)(_|-)";

	public static Long getTestCaseId(String testCaseSn) {
		Long testCaseId = null;
		if (StringUtils.hasLength(testCaseSn)) {
			testCaseId = Long.valueOf(testCaseSn.replaceAll(getTestCaseIdRegex, "").trim());
		}
		return testCaseId;
	}

	public static void main(String[] args) {
		//		String str = "@Test(description = \"人民币网关账户支付_API退款_过风控\", timeOut = 200000, groups = \"normal\",dependsOnMethods =  \"GetRepayCredit\" )";
		String str = "@Test(description = \"人民币网关账户支付_API退款_过风控\", timeOut = 200000, groups = \"normal\",dependsOnMethods =  {\"GetRepayCredit\" ,\"GetRepayCredit\"})";
		//		System.out.println(str.replaceAll(regexTestMethodPackage, ""));
		System.out.println(getTestMethodDescription(str));

		String classContenx = "package com.bill99.testmp.framework.helper;";
		System.out.println(getTestMethodPackage(classContenx));

		System.out.println(getDependsOnMethods(str));
	}
}
