package com.bill99.testmp.framework.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bill99.riaframework.common.dto.TaSvnMethodDto;

public class TaSvnHelper {

	private final static String regexStart = "@Test\\s*(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)";
	private final static String regexEnd = "@Test\\s*(\\(.*\\))?(\\s+)public(\\s+)(\\S+)(\\s+)(\\S+)\\(";
	private final static Pattern patternStart = Pattern.compile(regexStart);
	private final static Pattern patternEnd = Pattern.compile(regexEnd);

	public static List<TaSvnMethodDto> getTestMethod(String classContenx) {

		List<TaSvnMethodDto> taSvnMethodDtos = new ArrayList<TaSvnMethodDto>();
		TaSvnMethodDto taSvnMethodDto = null;
		Matcher mStart = patternStart.matcher(classContenx);
		Matcher mEnd = patternEnd.matcher(classContenx);
		while (mStart.find() && mEnd.find()) {
			String start = classContenx.substring(mStart.start(), mStart.end());
			String end = classContenx.substring(mEnd.start(), mEnd.end() - 1);
			taSvnMethodDto = new TaSvnMethodDto();
			taSvnMethodDto.setPackageName(RegexTaHelper.getTestMethodPackage(classContenx));
			taSvnMethodDto.setMethodName(end.substring(start.length(), end.length()));
			taSvnMethodDto.setDescription(RegexTaHelper.getTestMethodDescription(start));
			taSvnMethodDto.setDependsOnMethods(RegexTaHelper.getDependsOnMethods(start));
			taSvnMethodDtos.add(taSvnMethodDto);
		}
		return taSvnMethodDtos;
	}

}
