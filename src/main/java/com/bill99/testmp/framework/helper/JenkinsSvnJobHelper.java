package com.bill99.testmp.framework.helper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.bill99.testmp.testmanage.orm.ibatis.dto.JinkensJobMethodDto;
import com.bill99.testmp.testmanage.orm.manager.TaMng;

public class JenkinsSvnJobHelper {

	static SVNClientManager ourClientManager;
	private static SVNUpdateClient updateClient;
	private static SVNURL repositoryURL;

	public static void init(String url, String name, String password) {

		// 声明SVN客户端管理类
		// 初始化支持svn://协议的库。 必须先执行此操作。
		SVNRepositoryFactoryImpl.setup();
		// 相关变量赋值

		try {
			repositoryURL = SVNURL.parseURIEncoded(url);
		} catch (SVNException e) {
			//
		}
		ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
		// 实例化客户端管理类
		ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, name, password);
		updateClient = ourClientManager.getUpdateClient();
	}

	public static void parseJenkinsJobXml(String filePath, String xmlPath, String jobName, List<JinkensJobMethodDto> jinkensJobMethodDtos, TaMng taMng) throws SVNException,
			IOException {
		// checkout
		File wkFile = new File(filePath);
		updateClient.setIgnoreExternals(false);
		updateClient.doCheckout(repositoryURL, wkFile, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.FILES, false);

		File commitFile = new File(xmlPath);

		FileUtils.writeStringToFile(commitFile, JenkinsSchemaHelper.parseJenkinsJobXml(jobName, jinkensJobMethodDtos, taMng), "utf-8");

		// 执行更新操作
		long versionNum = updateClient.doUpdate(commitFile, SVNRevision.HEAD, SVNDepth.INFINITY, false, false);
		System.out.println("工作副本更新后的版本：" + versionNum);

		SVNStatus status = ourClientManager.getStatusClient().doStatus(commitFile, true);
		// 如果此文件是新增加的则先把此文件添加到版本库，然后提交。
		if (status == null || status.getContentsStatus() == SVNStatusType.STATUS_UNVERSIONED) {
			// 把此文件增加到版本库中
			ourClientManager.getWCClient().doAdd(commitFile, false, false, false, SVNDepth.INFINITY, false, false);
			// 提交此文件
			ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, "", null, null, true, false, SVNDepth.INFINITY);
			System.out.println("add");
		}
		// 如果此文件不是新增加的，直接提交。
		else {
			ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, "测试frankie", null, null, true, false, SVNDepth.INFINITY);
			System.out.println("commit");
		}
	}
}
