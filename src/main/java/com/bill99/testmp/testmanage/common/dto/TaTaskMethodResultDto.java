package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class TaTaskMethodResultDto implements Serializable {

	private static final long serialVersionUID = 716209694795802794L;

	private Long idTaTask;
	private Long idSvnMethod;
	private Boolean state;
	private String errorMsg;
	private Long costMillis;

	private String methodName;
	private String methodMemo;
	private String className;
	private String packageName;
	private String filePath;

	private Long idTestCase;
	private String testCaseName;

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Long getCostMillis() {
		return costMillis;
	}

	public void setCostMillis(Long costMillis) {
		this.costMillis = costMillis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSvnMethod == null) ? 0 : idSvnMethod.hashCode());
		result = prime * result + ((idTaTask == null) ? 0 : idTaTask.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaTaskMethodResultDto other = (TaTaskMethodResultDto) obj;
		if (idSvnMethod == null) {
			if (other.idSvnMethod != null)
				return false;
		} else if (!idSvnMethod.equals(other.idSvnMethod))
			return false;
		if (idTaTask == null) {
			if (other.idTaTask != null)
				return false;
		} else if (!idTaTask.equals(other.idTaTask))
			return false;
		return true;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodMemo() {
		return methodMemo;
	}

	public void setMethodMemo(String methodMemo) {
		this.methodMemo = methodMemo;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Long getIdTestCase() {
		return idTestCase;
	}

	public void setIdTestCase(Long idTestCase) {
		this.idTestCase = idTestCase;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

}
