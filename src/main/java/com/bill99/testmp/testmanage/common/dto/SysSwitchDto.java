package com.bill99.testmp.testmanage.common.dto;

import java.io.Serializable;

public class SysSwitchDto implements Serializable {

	private static final long serialVersionUID = 169726146001991045L;
	private Boolean notifyTaState;
	private String notifyTaRecvaddress;
	private String notifyTaCcaddress;

	public Boolean getNotifyTaState() {
		return notifyTaState;
	}

	public void setNotifyTaState(Boolean notifyTaState) {
		this.notifyTaState = notifyTaState;
	}

	public String getNotifyTaRecvaddress() {
		return notifyTaRecvaddress;
	}

	public void setNotifyTaRecvaddress(String notifyTaRecvaddress) {
		this.notifyTaRecvaddress = notifyTaRecvaddress;
	}

	public String getNotifyTaCcaddress() {
		return notifyTaCcaddress;
	}

	public void setNotifyTaCcaddress(String notifyTaCcaddress) {
		this.notifyTaCcaddress = notifyTaCcaddress;
	}

}
