package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.domain.TaSvnService;

public class TaPathSyncJobExecutor {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaSvnService taSvnService;

	public void execute() {
		logger.info("==>	initTaSvnPath	<==@*start*");
		taSvnService.initTaSvnPath();
		logger.info("==>	initTaSvnPath	<==@*success*");
	}

	public void setTaSvnService(TaSvnService taSvnService) {
		this.taSvnService = taSvnService;
	}

}
