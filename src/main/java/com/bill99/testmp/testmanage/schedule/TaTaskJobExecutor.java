package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.domain.TaTaskTiggerService;

public class TaTaskJobExecutor {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaTaskTiggerService taTaskTiggerService;

	public void execute() {
		logger.info("==>	checkTaskTigger	<==@*start*");
		taTaskTiggerService.checkTaskTigger();
		logger.info("==>	checkTaskTigger	<==@*success*");
	}

	public void setTaTaskTiggerService(TaTaskTiggerService taTaskTiggerService) {
		this.taTaskTiggerService = taTaskTiggerService;
	}

}
