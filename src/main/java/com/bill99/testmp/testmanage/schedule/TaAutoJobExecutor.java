package com.bill99.testmp.testmanage.schedule;

import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.testmanage.domain.TaTaskTiggerService;

public class TaAutoJobExecutor {

	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaTaskTiggerService taTaskTiggerService;

	public void execute() {
		logger.info("==>	autoTiggerTask	<==@*start*");
		taTaskTiggerService.autoTiggerTask();
		logger.info("==>	autoTiggerTask	<==@*success*");
	}

	public void setTaTaskTiggerService(TaTaskTiggerService taTaskTiggerService) {
		this.taTaskTiggerService = taTaskTiggerService;
	}

}
