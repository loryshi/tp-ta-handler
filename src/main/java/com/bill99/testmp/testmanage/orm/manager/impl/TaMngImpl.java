package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.TaEmailDto;
import com.bill99.testmp.testmanage.common.dto.SysSwitchDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TaIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dto.AutoTiggerTaskDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.CaseStateDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.JinkensJobMethodDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestProjectDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestTaTaskLogDto;
import com.bill99.testmp.testmanage.orm.manager.TaMng;

public class TaMngImpl implements TaMng {
	private TaIbatisDao taIbatisDao;

	public void setTaIbatisDao(TaIbatisDao taIbatisDao) {
		this.taIbatisDao = taIbatisDao;
	}

	public Long[] getTcIds(Long idSvnMethod) {
		List<Long> tcIds = taIbatisDao.getTcIds(idSvnMethod);
		if (tcIds != null) {
			return tcIds.toArray(new Long[] {});
		}
		return null;
	}

	public Long getTaMethodId(Map paramMap) {
		return taIbatisDao.getTaMethodId(paramMap);
	}

	public boolean isSuccess(Long idBuild) {
		Long count = taIbatisDao.getUnSuccCount(idBuild);

		if (count != null && count.intValue() > 0) {
			return true;
		}
		return false;
	}

	public List<JinkensJobMethodDto> getJinkensJobMethodsByTask(Long idTaTask) {
		return taIbatisDao.getJinkensJobMethodsByTask(idTaTask);
	}

	public void updatePlanTc(Long idTaTask, Long planStepId) {
		taIbatisDao.updatePlanTc(idTaTask, planStepId);
	}

	public CaseStateDto getCaseState(Long planStepId) {
		return taIbatisDao.getCaseState(planStepId);
	}

	public void updateCaseState(CaseStateDto caseStateDto) {
		taIbatisDao.updateCaseState(caseStateDto);
	}

	public JinkensJobMethodDto getJinkensJobMethodsByName(JinkensJobMethodDto jinkensJobMethodDto) {
		return taIbatisDao.getJinkensJobMethodsByName(jinkensJobMethodDto);
	}

	public List<AutoTiggerTaskDto> getTcIdsByCycleProject(Long teamId, String cycleId) {
		return taIbatisDao.getTcIdsByCycleProject(teamId, cycleId);
	}

	public List<TestProjectDto> findTestProjects() {
		return taIbatisDao.findTestProjects();
	}

	public void insertTaTaskLog(TestTaTaskLogDto testTaTaskLogDto) {
		taIbatisDao.insertTaTaskLog(testTaTaskLogDto);
	}

	public List<Long> getTcIdsByProjectRegression(Long teemId) {
		return taIbatisDao.getTcIdsByProjectRegression(teemId);
	}

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(String taTaskId, Long planStepId) {
		Map paraMap = new HashMap();
		paraMap.put("taTaskId", taTaskId);
		paraMap.put("planStepId", planStepId);
		return taIbatisDao.getTaTaskMethodResult(paraMap);
	}

	public List<TaResultDto> getTaResult(Long idTaTask, Long idSvnMethod) {
		Map paraMap = new HashMap();
		paraMap.put("idTaTask", idTaTask);
		paraMap.put("idSvnMethod", idSvnMethod);
		return taIbatisDao.getTaResult(paraMap);
	}

	public TestProjectDto findTestProjectById(Long testProjectId) {
		return taIbatisDao.findTestProjectById(testProjectId);
	}

	public SysSwitchDto getSysSwitch() {
		return taIbatisDao.getSysSwitch();
	}

	public Long getTcStatusCounts(String taTaskId, Long planStepId, String type) {
		return taIbatisDao.getTcStatusCounts(taTaskId, planStepId, type);
	}

	public TaEmailDto findCurrentUserByTaTask(String taTaskId) {
		return taIbatisDao.findCurrentUserByTaTask(taTaskId);
	}

	public Long findTab(Long projectId) {
		return taIbatisDao.findTab(projectId);
	}

}
