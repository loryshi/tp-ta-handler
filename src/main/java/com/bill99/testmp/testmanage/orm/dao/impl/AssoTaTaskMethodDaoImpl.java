package com.bill99.testmp.testmanage.orm.dao.impl;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.AssoTaTaskMethodDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskMethod;

public class AssoTaTaskMethodDaoImpl extends BaseDaoSupport<AssoTaTaskMethod, String> implements AssoTaTaskMethodDao {

	private final String getByIdHql = "from AssoTaTaskMethod where idTaTask=? and idSvnMethod=?";

	public AssoTaTaskMethod getById(Long idTaTask, Long idSvnMethod) {
		return findUnique(getByIdHql, idTaTask, idSvnMethod);
	}

}
