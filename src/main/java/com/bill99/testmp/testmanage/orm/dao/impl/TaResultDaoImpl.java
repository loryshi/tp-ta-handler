package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TaResultDao;
import com.bill99.testmp.testmanage.orm.entity.TaResult;

public class TaResultDaoImpl extends BaseDaoSupport<TaResult, Long> implements TaResultDao {

	private final String queryFailCountHql = "select count(r.idResult) from TaResult r ,TaBuild b where r.idBuild=b.id and r.state != 1 and b.buildId=? and b.buildNum =?";

	public Integer queryFailCount(String buildId, String buildNumber) {
		return ((Long) findUnique(queryFailCountHql, buildId, buildNumber)).intValue();
	}

	private final String queryByIdBuildHql = "from TaResult where idBuild=? order by idResult asc";

	public List<TaResult> queryByIdBuild(Long idBuild) {
		return find(queryByIdBuildHql, idBuild);
	}

}
