package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class TaSvnMethod implements Serializable {

	private static final long serialVersionUID = 4119226708422494282L;

	private Long idSvnMethod;
	private Long idSvnClass;
	private String methodName;
	private String memo;
	private String dependsOnMethods;

	public Long getIdSvnClass() {
		return idSvnClass;
	}

	public void setIdSvnClass(Long idSvnClass) {
		this.idSvnClass = idSvnClass;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSvnClass == null) ? 0 : idSvnClass.hashCode());
		result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaSvnMethod other = (TaSvnMethod) obj;
		if (idSvnClass == null) {
			if (other.idSvnClass != null)
				return false;
		} else if (!idSvnClass.equals(other.idSvnClass))
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		return true;
	}

	public String getDependsOnMethods() {
		return dependsOnMethods;
	}

	public void setDependsOnMethods(String dependsOnMethods) {
		this.dependsOnMethods = dependsOnMethods;
	}

}
