package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.TaEmailDto;
import com.bill99.testmp.testmanage.common.dto.SysSwitchDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.AutoTiggerTaskDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.CaseStateDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.JinkensJobMethodDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestProjectDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestTaTaskLogDto;

public interface TaMng {

	public Long[] getTcIds(Long idSvnMethod);

	public Long getTaMethodId(Map paramMap);

	public boolean isSuccess(Long idBuild);

	public List<JinkensJobMethodDto> getJinkensJobMethodsByTask(Long idTaTask);

	public void updatePlanTc(Long idTaTask, Long planStepId);

	public CaseStateDto getCaseState(Long planStepId);

	public void updateCaseState(CaseStateDto caseStateDto);

	public JinkensJobMethodDto getJinkensJobMethodsByName(JinkensJobMethodDto jinkensJobMethodDto);

	public List<AutoTiggerTaskDto> getTcIdsByCycleProject(Long teamId, String cycleId);

	public List<TestProjectDto> findTestProjects();

	public void insertTaTaskLog(TestTaTaskLogDto testTaTaskLogDto);

	public List<Long> getTcIdsByProjectRegression(Long teemId);

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(String taTaskId, Long planStepId);

	public List<TaResultDto> getTaResult(Long idTaTask, Long idSvnMethod);

	public TestProjectDto findTestProjectById(Long testProjectId);

	public SysSwitchDto getSysSwitch();

	public Long getTcStatusCounts(String taTaskId, Long planStepId, String type);
	
	public TaEmailDto findCurrentUserByTaTask(String taTaskId);
	
	public Long findTab(Long projectId);

}
