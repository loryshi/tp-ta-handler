package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TaProjectDao;
import com.bill99.testmp.testmanage.orm.entity.TaProject;

public class TaProjectDaoImpl extends BaseDaoSupport<TaProject, Long> implements TaProjectDao {

	private static final String find4ListHql = "from TaProject";//where testProjectId=72

	public List<TaProject> find4List() {
		return find(find4ListHql);
	}

	private static final String getTaProjectHql = "from TaProject where jobName=? ";

	public TaProject getTaProject(String jobName) {
		return findUnique(getTaProjectHql, jobName);
	}

}
