package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class AssoTaTaskTestCase implements Serializable {

	private static final long serialVersionUID = 816332550045365005L;

	private Long idTestCase;
	private Long idTaTask;

	public Long getIdTestCase() {
		return idTestCase;
	}

	public void setIdTestCase(Long idTestCase) {
		this.idTestCase = idTestCase;
	}

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

}
