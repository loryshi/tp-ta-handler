package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TaJob implements Serializable {

	private static final long serialVersionUID = -991633920033572778L;

	private String jobName;
	private Integer state;
	private Date finishDate;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

}
