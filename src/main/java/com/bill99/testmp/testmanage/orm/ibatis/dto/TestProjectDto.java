package com.bill99.testmp.testmanage.orm.ibatis.dto;

import java.io.Serializable;

public class TestProjectDto implements Serializable {

	private static final long serialVersionUID = 313082216211708428L;
	private Long testProjectId;
	private Long teemId;
	private String projectname;

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Long getTeemId() {
		return teemId;
	}

	public void setTeemId(Long teemId) {
		this.teemId = teemId;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

}
