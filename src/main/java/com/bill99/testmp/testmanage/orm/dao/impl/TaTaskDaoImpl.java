package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TaTaskDao;
import com.bill99.testmp.testmanage.orm.entity.TaTask;

public class TaTaskDaoImpl extends BaseDaoSupport<TaTask, String> implements TaTaskDao {

	private static final String getOnTimeTaskHql = "from TaTask where state=true and  isActual=false and triggerTime <= now()";

	public List<TaTask> getOnTimeTask() {
		return find(getOnTimeTaskHql);
	}

	private static final String getTaTaskHql = "from TaTask where taTaskId = ? and  testPlanId = ? and testProjectId = ?";

	public TaTask getTaTask(String taTaskId, Long testPlanId, Long idTestProject) {
		return findUnique(getTaTaskHql, taTaskId, testPlanId, idTestProject);
	}

	private static final String getTaTaskByIdHql = "from TaTask where idTaTask = ?";

	public TaTask getTaTask(Long idTaTask) {
		return findUnique(getTaTaskByIdHql, idTaTask);
	}

	private static final String getLastTaskByStepIdHql = "from TaTask where planStepId = ? order by idTaTask desc";

	public TaTask getLastTaskByStepId(Long planStepId) {
		return findUnique(getLastTaskByStepIdHql, planStepId);
	}

}
