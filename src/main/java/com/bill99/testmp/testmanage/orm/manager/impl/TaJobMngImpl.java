package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaJobDao;
import com.bill99.testmp.testmanage.orm.entity.TaJob;
import com.bill99.testmp.testmanage.orm.manager.TaJobMng;

public class TaJobMngImpl implements TaJobMng {

	private TaJobDao taJobDao;

	public void setTaJobDao(TaJobDao taJobDao) {
		this.taJobDao = taJobDao;
	}

	public TaJob queryByName(String jobName) {
		return taJobDao.queryByName(jobName);
	}

	public void save(TaJob taJob) {
		taJobDao.save(taJob);
	}

	public void update(TaJob taJob) {
		taJobDao.update(taJob);
	}

	public List<TaJob> list() {
		return taJobDao.getAll();
	}

	public TaJob merge(TaJob taJob) {
		return taJobDao.merge(taJob);
	}

}
