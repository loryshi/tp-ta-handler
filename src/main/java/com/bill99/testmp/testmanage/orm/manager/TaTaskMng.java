package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaTask;

public interface TaTaskMng {

	public List<TaTask> getOnTimeTask();

	public void update(TaTask taTask);

	public TaTask save(TaTask taTask);

	public TaTask getTaTask(String taTaskId, Long testPlanId, Long idTestProject);

	public TaTask getTaTask(Long idTaTask);

	public TaTask getLastTaskByStepId(Long planStepId);

}
