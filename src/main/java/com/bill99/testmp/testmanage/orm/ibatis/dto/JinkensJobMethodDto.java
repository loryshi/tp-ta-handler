package com.bill99.testmp.testmanage.orm.ibatis.dto;

import java.io.Serializable;

public class JinkensJobMethodDto implements Serializable {

	private static final long serialVersionUID = -6095792651269621823L;
	private Long idTaProject;
	private String jobName;
	private String packageName;
	private String className;
	private String methodName;
	private String dependsOnMethods;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Long getIdTaProject() {
		return idTaProject;
	}

	public void setIdTaProject(Long idTaProject) {
		this.idTaProject = idTaProject;
	}

	public String getDependsOnMethods() {
		return dependsOnMethods;
	}

	public void setDependsOnMethods(String dependsOnMethods) {
		this.dependsOnMethods = dependsOnMethods;
	}

}
