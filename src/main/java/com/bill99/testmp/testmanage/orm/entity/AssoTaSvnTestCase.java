package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class AssoTaSvnTestCase implements Serializable {

	private static final long serialVersionUID = -5431553469113457466L;

	private Long testCaseId;
	private Long idSvnMethod;

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

}
