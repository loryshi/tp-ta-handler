package com.bill99.testmp.testmanage.orm.ibatis.dto;

import java.io.Serializable;

public class AutoTiggerTaskDto implements Serializable {

	private static final long serialVersionUID = -63242487188360665L;

	private Long testPlanId;//测试计划ID
	private Long planStepId;//测试计划步骤ID
	private Long tcId;//测试用例ID

	public Long getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(Long testPlanId) {
		this.testPlanId = testPlanId;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getTcId() {
		return tcId;
	}

	public void setTcId(Long tcId) {
		this.tcId = tcId;
	}

}
