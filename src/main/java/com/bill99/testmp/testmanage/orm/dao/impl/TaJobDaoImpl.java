package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TaJobDao;
import com.bill99.testmp.testmanage.orm.entity.TaJob;

public class TaJobDaoImpl extends BaseDaoSupport<TaJob, String> implements TaJobDao {

	private final String queryByNameHql = "from TaJob where jobName = ?";

	public TaJob queryByName(String jobName) {
		return findUnique(queryByNameHql, jobName);
	}

	public List<TaJob> list() {
		return list();
	}

}
