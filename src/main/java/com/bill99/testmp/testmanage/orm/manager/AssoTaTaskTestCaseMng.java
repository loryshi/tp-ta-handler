package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

public interface AssoTaTaskTestCaseMng {

	public int bath4insert(Long[] tcIds, Long idTaTask);

	public void insertTaTaskTcs4Batch(Long idTaTask, Long[] idTestCases);

	public List<Long> findTestTaTaskProjects(Long idTestTaTask);

	public List<Long> findTestTaTaskTcs(Long idTestTaTask, Long idTestProject);

}
