package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaJob;

public interface TaJobMng {

	public TaJob queryByName(String jobName);

	public void save(TaJob taJob);

	public void update(TaJob taJob);

	public List<TaJob> list();

	public TaJob merge(TaJob taJob);

}
