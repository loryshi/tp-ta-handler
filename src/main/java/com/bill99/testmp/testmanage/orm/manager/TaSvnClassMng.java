package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaSvnClass;

public interface TaSvnClassMng {

	public int bath4merge(List<TaSvnClass> svnTaPaths, Long idTaProject);

	public int bath4mergeMethod(List<TaSvnClass> svnTaPaths, Long idTaProject);

	public TaSvnClass query(String packageName, String className, Long testProjectId);

}
