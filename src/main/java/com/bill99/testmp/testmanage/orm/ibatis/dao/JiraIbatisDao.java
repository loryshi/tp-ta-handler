package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;

public interface JiraIbatisDao {

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds);

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId);

}
