package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaSvnClassDao;
import com.bill99.testmp.testmanage.orm.entity.TaSvnClass;
import com.bill99.testmp.testmanage.orm.manager.TaSvnClassMng;

public class TaSvnClassMngImpl implements TaSvnClassMng {

	private TaSvnClassDao taSvnClassDao;

	public int bath4merge(List<TaSvnClass> svnTaPaths, Long idTaProject) {
		List<Long> originalIds = taSvnClassDao.bath4merge(svnTaPaths, idTaProject);
		if (originalIds != null && originalIds.size() > 0) {
			for (Long originalId : originalIds) {
				taSvnClassDao.deleteClass(originalId);
			}
		}
		return originalIds == null ? 0 : originalIds.size();
	}

	public TaSvnClass query(String packageName, String className, Long testProjectId) {
		return taSvnClassDao.query(packageName, className, testProjectId);
	}

	public void setTaSvnClassDao(TaSvnClassDao taSvnClassDao) {
		this.taSvnClassDao = taSvnClassDao;
	}

	public int bath4mergeMethod(List<TaSvnClass> svnTaPaths, Long idTaProject) {
		List<Long> originalIds = taSvnClassDao.bath4mergeMethod(svnTaPaths, idTaProject);
		if (originalIds != null && originalIds.size() > 0) {
			for (Long originalId : originalIds) {
				taSvnClassDao.deleteMethod(originalId);
			}
		}
		return originalIds == null ? 0 : originalIds.size();
	}

}
