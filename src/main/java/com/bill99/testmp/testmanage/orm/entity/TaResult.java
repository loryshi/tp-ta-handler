package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TaResult implements Serializable {

	private static final long serialVersionUID = 3632736451718971549L;

	private Long idResult;
	private Long idBuild;

	private String memo;
	private String errorMsg;
	private Boolean state;
	private Date createDate;

	private Long idSvnMethod;
	private Integer type;

	/**
	 * @return the idResult
	 */
	public Long getIdResult() {
		return idResult;
	}

	/**
	 * @param idResult the idResult to set
	 */
	public void setIdResult(Long idResult) {
		this.idResult = idResult;
	}

	/**
	 * @return the idBuild
	 */
	public Long getIdBuild() {
		return idBuild;
	}

	/**
	 * @param idBuild the idBuild to set
	 */
	public void setIdBuild(Long idBuild) {
		this.idBuild = idBuild;
	}

	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * @return the state
	 */
	public Boolean getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Boolean state) {
		this.state = state;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
