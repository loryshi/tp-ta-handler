package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaProject;

public interface TaProjectMng {

	List<TaProject> find4List();

	TaProject getTaProject(String jobName);

}
