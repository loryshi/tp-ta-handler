package com.bill99.testmp.testmanage.orm.dao.impl;

import java.util.List;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.TaBuildDao;
import com.bill99.testmp.testmanage.orm.entity.TaBuild;

public class TaBuildDaoImpl extends BaseDaoSupport<TaBuild, String> implements TaBuildDao {

	private final String query4rpHql = "from TaBuild where jobName=? and buildId=? and buildNum=?";

	public TaBuild query4rp(String jobName, String buildId, String buildNumber) {
		return findUnique(query4rpHql, jobName, buildId, buildNumber);
	}

	private final String queryByJobHql = "from TaBuild where jobName=? order by createDate desc";

	public List<TaBuild> queryByJob(String jobName) {
		return find(queryByJobHql, jobName);
	}

	private final String getTaBuildHql = "from TaBuild where idTaTask=? and testPlanId=? order by buildNum desc";

	public TaBuild getTaBuild(Long idTaTask, Long testPlanId) {
		return findUnique(getTaBuildHql, idTaTask, testPlanId);
	}

	private final String getIdTaProjectHql = "select tp.idTaProject from TaProject tp,TaTask tt where tt.testProjectId=tp.idTestProject and tt.idTaTask=? and tp.jobName=? ";

	public Long getIdTaProject(Long idTaTask, String jobName) {
		return findUnique(getIdTaProjectHql, idTaTask, jobName);
	}

}
