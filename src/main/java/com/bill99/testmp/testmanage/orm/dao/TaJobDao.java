package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaJob;

public interface TaJobDao extends BaseDao<TaJob, String> {

	public TaJob queryByName(String jobName);

	public List<TaJob> list();

}
