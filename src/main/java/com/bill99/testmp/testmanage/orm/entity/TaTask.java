package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TaTask implements Serializable {

	private static final long serialVersionUID = 4966864491536929845L;
	private Long idTaTask;

	private String taTaskId;
	private Long testPlanId;
	private Long testProjectId;
	private Long planStepId;

	private Boolean isActual;
	private Date triggerTime;
	private Boolean state;
	private Date updateDate;
	private Date createDate;

	private Long idTestTaTask;//测试平台触发任务ID

	public String getTaTaskId() {
		return taTaskId;
	}

	public void setTaTaskId(String taTaskId) {
		this.taTaskId = taTaskId;
	}

	public Long getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(Long testPlanId) {
		this.testPlanId = testPlanId;
	}

	public Boolean getIsActual() {
		return isActual;
	}

	public void setIsActual(Boolean isActual) {
		this.isActual = isActual;
	}

	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taTaskId == null) ? 0 : taTaskId.hashCode());
		result = prime * result + ((testPlanId == null) ? 0 : testPlanId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaTask other = (TaTask) obj;
		if (taTaskId == null) {
			if (other.taTaskId != null)
				return false;
		} else if (!taTaskId.equals(other.taTaskId))
			return false;
		if (testPlanId == null) {
			if (other.testPlanId != null)
				return false;
		} else if (!testPlanId.equals(other.testPlanId))
			return false;
		return true;
	}

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public Long getTestProjectId() {
		return testProjectId;
	}

	public void setTestProjectId(Long testProjectId) {
		this.testProjectId = testProjectId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getPlanStepId() {
		return planStepId;
	}

	public void setPlanStepId(Long planStepId) {
		this.planStepId = planStepId;
	}

	public Long getIdTestTaTask() {
		return idTestTaTask;
	}

	public void setIdTestTaTask(Long idTestTaTask) {
		this.idTestTaTask = idTestTaTask;
	}

}
