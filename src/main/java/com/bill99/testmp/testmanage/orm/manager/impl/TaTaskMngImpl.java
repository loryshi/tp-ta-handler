package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaTaskDao;
import com.bill99.testmp.testmanage.orm.entity.TaTask;
import com.bill99.testmp.testmanage.orm.manager.TaTaskMng;

public class TaTaskMngImpl implements TaTaskMng {
	private TaTaskDao taTaskDao;

	public void setTaTaskDao(TaTaskDao taTaskDao) {
		this.taTaskDao = taTaskDao;
	}

	public List<TaTask> getOnTimeTask() {
		return taTaskDao.getOnTimeTask();
	}

	public void update(TaTask taTask) {
		taTaskDao.update(taTask);
	}

	public TaTask getTaTask(String taTaskId, Long testPlanId, Long idTestProject) {
		return taTaskDao.getTaTask(taTaskId, testPlanId, idTestProject);
	}

	public TaTask save(TaTask taTask) {
		return taTaskDao.save(taTask);
	}

	public TaTask getTaTask(Long idTaTask) {
		return taTaskDao.getTaTask(idTaTask);
	}

	public TaTask getLastTaskByStepId(Long planStepId) {
		return taTaskDao.getLastTaskByStepId(planStepId);
	}

}
