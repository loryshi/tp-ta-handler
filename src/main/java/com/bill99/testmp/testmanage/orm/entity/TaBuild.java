package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TaBuild implements Serializable {

	private static final long serialVersionUID = 942131730133568534L;

	private Long idBuild;
	private Long idTaTask;

	private String jobName;//job
	private String buildNum;
	private String buildId;

	private String memo;
	private String errorMsg;//错误信息
	private Date finishDate;
	private Integer state;
	private Date createDate;

	private Long idMethod;

	/**
	 * @return the idBuild
	 */
	public Long getIdBuild() {
		return idBuild;
	}

	/**
	 * @param idBuild the idBuild to set
	 */
	public void setIdBuild(Long idBuild) {
		this.idBuild = idBuild;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the buildNum
	 */
	public String getBuildNum() {
		return buildNum;
	}

	/**
	 * @param buildNum the buildNum to set
	 */
	public void setBuildNum(String buildNum) {
		this.buildNum = buildNum;
	}

	/**
	 * @return the buildId
	 */
	public String getBuildId() {
		return buildId;
	}

	/**
	 * @param buildId the buildId to set
	 */
	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return the finishDate
	 */
	public Date getFinishDate() {
		return finishDate;
	}

	/**
	 * @param finishDate the finishDate to set
	 */
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public Long getIdMethod() {
		return idMethod;
	}

	public void setIdMethod(Long idMethod) {
		this.idMethod = idMethod;
	}

}
