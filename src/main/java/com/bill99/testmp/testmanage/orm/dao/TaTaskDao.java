package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaTask;

public interface TaTaskDao extends BaseDao<TaTask, String> {

	public List<TaTask> getOnTimeTask();

	public TaTask getTaTask(String taTaskId, Long testPlanId, Long idTestProject);

	public TaTask getTaTask(Long idTaTask);

	public TaTask getLastTaskByStepId(Long planStepId);

}
