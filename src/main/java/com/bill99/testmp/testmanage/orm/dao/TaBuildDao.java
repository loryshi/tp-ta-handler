package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaBuild;

public interface TaBuildDao extends BaseDao<TaBuild, String> {

	public TaBuild query4rp(String jobName, String buildId, String buildNumber);

	public List<TaBuild> queryByJob(String jobName);

	public TaBuild getTaBuild(Long idTaTask, Long testPlanId);

	public Long getIdTaProject(Long idTaTask, String jobName);

}
