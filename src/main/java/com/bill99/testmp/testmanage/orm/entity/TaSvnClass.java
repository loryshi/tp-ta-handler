package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.List;

public class TaSvnClass implements Serializable {

	private static final long serialVersionUID = -6730274061155176816L;

	private Long idSvnClass;
	private String filePath;
	private String packageName;
	private String className;
	private Long idTaProject;
	private String memo;
	private List<TaSvnMethod> taSvnMethods;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result + ((idTaProject == null) ? 0 : idTaProject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaSvnClass other = (TaSvnClass) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (idTaProject == null) {
			if (other.idTaProject != null)
				return false;
		} else if (!idTaProject.equals(other.idTaProject))
			return false;
		return true;
	}

	public Long getIdSvnClass() {
		return idSvnClass;
	}

	public void setIdSvnClass(Long idSvnClass) {
		this.idSvnClass = idSvnClass;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public List<TaSvnMethod> getTaSvnMethods() {
		return taSvnMethods;
	}

	public void setTaSvnMethods(List<TaSvnMethod> taSvnMethods) {
		this.taSvnMethods = taSvnMethods;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Long getIdTaProject() {
		return idTaProject;
	}

	public void setIdTaProject(Long idTaProject) {
		this.idTaProject = idTaProject;
	}

}
