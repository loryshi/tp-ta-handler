package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraIbatisDao;

public class JiraIbatisDaoImpl extends QueryDaoSupport implements JiraIbatisDao {

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds) {
		Map<String, Long[]> map = new HashMap<String, Long[]>();
		map.put("roleIds", roleIds);
		map.put("teemIds", teemIds);
		return super.queryForList("jira.getEmail4Role", map);
	}

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId) {
		Map map = new HashMap();
		map.put("roleIssueIds", roleIssueIds);
		map.put("issueId", issueId);
		return super.queryForList("jira.getEmail4RoleIssue", map);
	}

}
