package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;

public class AssoTaTaskMethod implements Serializable {

	private static final long serialVersionUID = 716209694795802794L;

	private Long idTaTask;
	private Long idSvnMethod;
	private Boolean state;
	private String errorMsg;
	private Long costMillis;

	public Long getIdTaTask() {
		return idTaTask;
	}

	public void setIdTaTask(Long idTaTask) {
		this.idTaTask = idTaTask;
	}

	public Long getIdSvnMethod() {
		return idSvnMethod;
	}

	public void setIdSvnMethod(Long idSvnMethod) {
		this.idSvnMethod = idSvnMethod;
	}

	public Boolean getState() {
		return state;
	}

	public void setState(Boolean state) {
		this.state = state;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Long getCostMillis() {
		return costMillis;
	}

	public void setCostMillis(Long costMillis) {
		this.costMillis = costMillis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSvnMethod == null) ? 0 : idSvnMethod.hashCode());
		result = prime * result + ((idTaTask == null) ? 0 : idTaTask.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssoTaTaskMethod other = (AssoTaTaskMethod) obj;
		if (idSvnMethod == null) {
			if (other.idSvnMethod != null)
				return false;
		} else if (!idSvnMethod.equals(other.idSvnMethod))
			return false;
		if (idTaTask == null) {
			if (other.idTaTask != null)
				return false;
		} else if (!idTaTask.equals(other.idTaTask))
			return false;
		return true;
	}

}
