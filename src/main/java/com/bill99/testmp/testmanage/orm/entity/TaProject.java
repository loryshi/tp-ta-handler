package com.bill99.testmp.testmanage.orm.entity;

import java.io.Serializable;
import java.util.Date;

public class TaProject implements Serializable {

	private static final long serialVersionUID = -9039026877658245772L;

	private Long idTaProject;
	private Long idTestProject;
	private String taSvnUrl;
	private String jobName;
	private Date updateDate;
	private Date createDate;

	public Long getIdTaProject() {
		return idTaProject;
	}

	public void setIdTaProject(Long idTaProject) {
		this.idTaProject = idTaProject;
	}

	public Long getIdTestProject() {
		return idTestProject;
	}

	public void setIdTestProject(Long idTestProject) {
		this.idTestProject = idTestProject;
	}

	public String getTaSvnUrl() {
		return taSvnUrl;
	}

	public void setTaSvnUrl(String taSvnUrl) {
		this.taSvnUrl = taSvnUrl;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
