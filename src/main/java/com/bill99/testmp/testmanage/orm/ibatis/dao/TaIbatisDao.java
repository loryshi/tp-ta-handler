package com.bill99.testmp.testmanage.orm.ibatis.dao;

import java.util.List;
import java.util.Map;

import com.bill99.riaframework.common.dto.TaEmailDto;
import com.bill99.testmp.testmanage.common.dto.SysSwitchDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.AutoTiggerTaskDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.CaseStateDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.JinkensJobMethodDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestProjectDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestTaTaskLogDto;

public interface TaIbatisDao {

	public List<Long> getTcIds(Long idSvnMethod);

	public Long getTaMethodId(Map paramMap);

	public Long getUnSuccCount(Long idBuild);

	public List<JinkensJobMethodDto> getJinkensJobMethodsByTask(Long idTaTask);

	public void updatePlanTc(Long idTaTask, Long planStepId);

	public CaseStateDto getCaseState(Long planStepId);

	public void updateCaseState(CaseStateDto caseStateDto);

	public JinkensJobMethodDto getJinkensJobMethodsByName(JinkensJobMethodDto jinkensJobMethodDto);

	public List<AutoTiggerTaskDto> getTcIdsByCycleProject(Long teamId, String cycleId);

	public List<TestProjectDto> findTestProjects();

	public void insertTaTaskLog(TestTaTaskLogDto testTaTaskLogDto);

	public List<Long> getTcIdsByProjectRegression(Long teemId);

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(Map paraMap);

	public List<TaResultDto> getTaResult(Map paraMap);

	public TestProjectDto findTestProjectById(Long testProjectId);

	public SysSwitchDto getSysSwitch();

	public Long getTcStatusCounts(String taTaskId, Long planStepId, String type);

	public void insertTaTaskTcs4Batch(final Long idTaTask, final Long[] idTestCases);

	public List<Long> findTestTaTaskProjects(Long idTestTaTask);

	public List<Long> findTestTaTaskTcs(Long idTestTaTask, Long idTestProject);
	
    public TaEmailDto findCurrentUserByTaTask(String taTaskId);
    
    public Long findTab(Long projectId);
}
