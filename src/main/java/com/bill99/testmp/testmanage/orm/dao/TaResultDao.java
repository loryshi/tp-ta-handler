package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaResult;

public interface TaResultDao extends BaseDao<TaResult, Long> {

	Integer queryFailCount(String buildId, String buildNumber);

	List<TaResult> queryByIdBuild(Long idBuild);

}
