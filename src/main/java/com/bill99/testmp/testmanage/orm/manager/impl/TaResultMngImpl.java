package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaResultDao;
import com.bill99.testmp.testmanage.orm.entity.TaResult;
import com.bill99.testmp.testmanage.orm.manager.TaResultMng;

public class TaResultMngImpl implements TaResultMng {
	private TaResultDao taResultDao;

	public void setTaResultDao(TaResultDao taResultDao) {
		this.taResultDao = taResultDao;
	}

	public Integer queryFailCount(String buildId, String buildNumber) {
		return taResultDao.queryFailCount(buildId, buildNumber);
	}

	public TaResult save(TaResult taResult) {
		return taResultDao.save(taResult);
	}

	public List<TaResult> queryByIdBuild(Long idBuild) {
		return taResultDao.queryByIdBuild(idBuild);
	}

}
