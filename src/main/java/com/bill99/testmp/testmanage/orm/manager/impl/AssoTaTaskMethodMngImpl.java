package com.bill99.testmp.testmanage.orm.manager.impl;

import com.bill99.testmp.testmanage.orm.dao.AssoTaTaskMethodDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskMethod;
import com.bill99.testmp.testmanage.orm.manager.AssoTaTaskMethodMng;

public class AssoTaTaskMethodMngImpl implements AssoTaTaskMethodMng {

	private AssoTaTaskMethodDao assoTaTaskMethodDao;

	public void setAssoTaTaskMethodDao(AssoTaTaskMethodDao assoTaTaskMethodDao) {
		this.assoTaTaskMethodDao = assoTaTaskMethodDao;
	}

	public AssoTaTaskMethod merge(AssoTaTaskMethod assoTaTaskMethod) {
		return assoTaTaskMethodDao.merge(assoTaTaskMethod);
	}

	public AssoTaTaskMethod save(AssoTaTaskMethod assoTaTaskMethod) {
		return assoTaTaskMethodDao.save(assoTaTaskMethod);
	}

	public AssoTaTaskMethod getById(Long idTaTask, Long idSvnMethod) {
		return assoTaTaskMethodDao.getById(idTaTask, idSvnMethod);
	}

	public void update(AssoTaTaskMethod assoTaTaskMethod) {
		assoTaTaskMethodDao.update(assoTaTaskMethod);
	}

}
