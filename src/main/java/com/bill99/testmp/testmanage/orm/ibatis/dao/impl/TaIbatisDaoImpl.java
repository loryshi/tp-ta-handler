package com.bill99.testmp.testmanage.orm.ibatis.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.bill99.riaframework.common.dto.TaEmailDto;
import com.bill99.seashell.orm.ibatis.QueryDaoSupport;
import com.bill99.testmp.testmanage.common.dto.SysSwitchDto;
import com.bill99.testmp.testmanage.common.dto.TaResultDto;
import com.bill99.testmp.testmanage.common.dto.TaTaskMethodResultDto;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TaIbatisDao;
import com.bill99.testmp.testmanage.orm.ibatis.dto.AutoTiggerTaskDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.CaseStateDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.JinkensJobMethodDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestProjectDto;
import com.bill99.testmp.testmanage.orm.ibatis.dto.TestTaTaskLogDto;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class TaIbatisDaoImpl extends QueryDaoSupport implements TaIbatisDao {

	public List<Long> getTcIds(Long idSvnMethod) {
		return queryForList("TaTask.getTcIds", idSvnMethod);
	}

	public Long getTaMethodId(Map paramMap) {
		return queryForObject("TaReport.getTaMethodId", paramMap);
	}

	public Long getUnSuccCount(Long idBuild) {
		return queryForObject("TaReport.getUnSuccCount", idBuild);
	}

	public List<JinkensJobMethodDto> getJinkensJobMethodsByTask(Long idTaTask) {
		return queryForList("TaTask.getJinkensJobMethodsByTask", idTaTask);
	}

	public void updatePlanTc(Long idTaTask, Long planStepId) {
		Map<String, Long> paramMap = new HashMap<String, Long>();
		paramMap.put("idTaTask", idTaTask);
		paramMap.put("planStepId", planStepId);

		getSqlMapClientTemplate().update("TaTask.updatePlanTcSucc", paramMap);
		getSqlMapClientTemplate().update("TaTask.updatePlanTcFail", paramMap);
	}

	public CaseStateDto getCaseState(Long planStepId) {
		return queryForObject("TaTask.getCaseState", planStepId);
	}

	public void updateCaseState(CaseStateDto caseStateDto) {
		getSqlMapClientTemplate().update("TaTask.updateCaseState", caseStateDto);
	}

	public JinkensJobMethodDto getJinkensJobMethodsByName(JinkensJobMethodDto jinkensJobMethodDto) {
		return queryForObject("TaTask.getJinkensJobMethodsByName", jinkensJobMethodDto);
	}

	public List<AutoTiggerTaskDto> getTcIdsByCycleProject(Long teamId, String cycleId) {
		Map map = new HashMap();
		map.put("teamId", teamId);
		map.put("cycleId", cycleId);
		return queryForList("TaTask.getTcIdsByCycleProject", map);
	}

	public List<TestProjectDto> findTestProjects() {
		return queryForList("TaTask.findTestProjects", null);
	}

	public void insertTaTaskLog(TestTaTaskLogDto testTaTaskLogDto) {
		getSqlMapClientTemplate().insert("TaTask.insertTaTaskLog", testTaTaskLogDto);
	}

	public List<Long> getTcIdsByProjectRegression(Long teemId) {
		return queryForList("TaTask.getTcIdsByProjectRegression", teemId);
	}

	public List<TaTaskMethodResultDto> getTaTaskMethodResult(Map paraMap) {
		return queryForList("TaTask.getTaTaskMethodResult", paraMap);
	}

	public List<TaResultDto> getTaResult(Map paraMap) {
		return queryForList("TaTask.getTaResult", paraMap);
	}

	public TestProjectDto findTestProjectById(Long testProjectId) {
		return queryForObject("TaTask.findTestProjectById", testProjectId);
	}

	public SysSwitchDto getSysSwitch() {
		return queryForObject("TaTask.getSysSwitch", null);
	}

	public Long getTcStatusCounts(String taTaskId, Long planStepId, String type) {
		Map map = new HashMap();
		map.put("taTaskId", taTaskId);
		map.put("planStepId", planStepId);
		map.put("type", type);
		return queryForObject("TaTask.getTcStatusCounts", map);
	}

	public void insertTaTaskTcs4Batch(final Long idTaTask, final Long[] idTestCases) {
		this.getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				if (idTestCases != null) {
					Map paramMap = new HashMap();
					paramMap.put("idTaTask", idTaTask);
					for (Long idTestCase : idTestCases) {
						paramMap.put("idTestCase", idTestCase);
						executor.insert("TaTask.insertTaTaskTcs4Batch", paramMap);
					}
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public List<Long> findTestTaTaskProjects(Long idTestTaTask) {
		return queryForList("TaTask.findTestTaTaskProjects", idTestTaTask);
	}

	public List<Long> findTestTaTaskTcs(Long idTestTaTask, Long idTestProject) {
		Map<String, Long> paramMap = new HashMap<String, Long>();
		paramMap.put("idTestTaTask", idTestTaTask);
		paramMap.put("idTestProject", idTestProject);
		return queryForList("TaTask.findTestTaTaskTcs", paramMap);
	}

	public TaEmailDto findCurrentUserByTaTask(String taTaskId) {	
		return queryForObject("TaTask.findCurrentUserByTaTask", taTaskId);
	}

	public Long findTab(Long projectId) {
		return queryForObject("TaTask.findTab",projectId);
	}
}
