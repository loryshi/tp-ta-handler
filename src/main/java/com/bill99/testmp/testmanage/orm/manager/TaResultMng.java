package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaResult;

public interface TaResultMng {

	public Integer queryFailCount(String buildId, String buildNumber);

	public TaResult save(TaResult taResult);

	public List<TaResult> queryByIdBuild(Long idBuild);

}
