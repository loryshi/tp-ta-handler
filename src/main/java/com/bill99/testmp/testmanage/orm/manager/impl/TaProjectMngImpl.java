package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaProjectDao;
import com.bill99.testmp.testmanage.orm.entity.TaProject;
import com.bill99.testmp.testmanage.orm.manager.TaProjectMng;

public class TaProjectMngImpl implements TaProjectMng {

	private TaProjectDao taProjectDao;

	public void setTaProjectDao(TaProjectDao taProjectDao) {
		this.taProjectDao = taProjectDao;
	}

	public List<TaProject> find4List() {
		return taProjectDao.find4List();
	}

	public TaProject getTaProject(String jobName) {
		return taProjectDao.getTaProject(jobName);
	}

}
