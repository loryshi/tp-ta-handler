package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.AssoTaTaskTestCaseDao;
import com.bill99.testmp.testmanage.orm.ibatis.dao.TaIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.AssoTaTaskTestCaseMng;

public class AssoTaTaskTestCaseMngImpl implements AssoTaTaskTestCaseMng {
	private AssoTaTaskTestCaseDao assoTaTaskTestCaseDao;
	private TaIbatisDao taIbatisDao;

	public int bath4insert(Long[] tcIds, Long idTaTask) {
		return assoTaTaskTestCaseDao.insert4bath(tcIds, idTaTask);
	}

	public void insertTaTaskTcs4Batch(Long idTaTask, Long[] idTestCases) {
		taIbatisDao.insertTaTaskTcs4Batch(idTaTask, idTestCases);
	}

	public void setAssoTaTaskTestCaseDao(AssoTaTaskTestCaseDao assoTaTaskTestCaseDao) {
		this.assoTaTaskTestCaseDao = assoTaTaskTestCaseDao;
	}

	public void setTaIbatisDao(TaIbatisDao taIbatisDao) {
		this.taIbatisDao = taIbatisDao;
	}

	public List<Long> findTestTaTaskProjects(Long idTestTaTask) {
		return taIbatisDao.findTestTaTaskProjects(idTestTaTask);
	}

	public List<Long> findTestTaTaskTcs(Long idTestTaTask, Long idTestProject) {
		return taIbatisDao.findTestTaTaskTcs(idTestTaTask, idTestProject);
	}

}
