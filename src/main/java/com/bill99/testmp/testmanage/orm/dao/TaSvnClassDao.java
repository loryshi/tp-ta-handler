package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaSvnClass;

public interface TaSvnClassDao extends BaseDao<TaSvnClass, Long> {

	public List<Long> bath4merge(List<TaSvnClass> svnTaPaths, Long idTaProject);

	public List<Long> bath4mergeMethod(List<TaSvnClass> svnTaPaths, Long idTaProject);

	public void deleteClass(Long idSvnClass);

	public void deleteMethod(Long idSvnMethod);

	public TaSvnClass query(String packageName, String className, Long testProjectId);

}
