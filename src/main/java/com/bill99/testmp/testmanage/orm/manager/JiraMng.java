package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

public interface JiraMng {

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds);

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId);

}
