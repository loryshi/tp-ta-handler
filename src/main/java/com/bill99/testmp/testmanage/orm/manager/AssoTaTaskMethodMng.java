package com.bill99.testmp.testmanage.orm.manager;

import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskMethod;

public interface AssoTaTaskMethodMng {

	public AssoTaTaskMethod merge(AssoTaTaskMethod assoTaTaskMethod);

	public void update(AssoTaTaskMethod assoTaTaskMethod);

	public AssoTaTaskMethod save(AssoTaTaskMethod assoTaTaskMethod);

	public AssoTaTaskMethod getById(Long idTaTask, Long idSvnMethod);

}
