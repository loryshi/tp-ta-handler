package com.bill99.testmp.testmanage.orm.dao;

import java.util.List;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.TaProject;

public interface TaProjectDao extends BaseDao<TaProject, Long> {

	List<TaProject> find4List();

	TaProject getTaProject(String jobName);

}
