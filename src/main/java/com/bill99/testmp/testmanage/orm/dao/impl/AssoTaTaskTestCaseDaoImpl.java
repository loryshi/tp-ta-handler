package com.bill99.testmp.testmanage.orm.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.Session;

import com.bill99.seashell.orm.hibernate.BaseDaoSupport;
import com.bill99.testmp.testmanage.orm.dao.AssoTaTaskTestCaseDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskTestCase;

public class AssoTaTaskTestCaseDaoImpl extends BaseDaoSupport<AssoTaTaskTestCase, String> implements AssoTaTaskTestCaseDao {

	private static Log logger = LogFactory.getLog(AssoTaTaskTestCaseDaoImpl.class);

	private final int BATCH_IMPORT_SIZE = 50;

	public int bath4insert(Long[] tcIds, Long idTaTask) {
		AssoTaTaskTestCase assoTaTaskTestCase = null;
		Session session = getSession();
		session.setCacheMode(CacheMode.IGNORE);
		int rows = 0;
		if (tcIds.length == 0) {
			return rows;
		}
		session.beginTransaction();

		for (Long tcId : tcIds) {
			assoTaTaskTestCase = new AssoTaTaskTestCase();
			assoTaTaskTestCase.setIdTaTask(idTaTask);
			assoTaTaskTestCase.setIdTestCase(tcId);
			try {
				session.save(assoTaTaskTestCase);
				rows++;
			} catch (Exception e) {
				logger.error(e);
			}
			if (rows % BATCH_IMPORT_SIZE == 0) {
				session.flush();
				session.clear();
				session.getTransaction().commit();
			}

		}
		if (rows % BATCH_IMPORT_SIZE != 0) {
			session.flush();
			session.clear();
			session.getTransaction().commit();
		}
		releaseSession(session);
		logger.info("class rows = " + rows);
		return rows;

	}

	public int insert4bath(Long[] tcIds, Long idTaTask) {
		AssoTaTaskTestCase assoTaTaskTestCase = null;
		for (Long tcId : tcIds) {
			assoTaTaskTestCase = new AssoTaTaskTestCase();
			assoTaTaskTestCase.setIdTaTask(idTaTask);
			assoTaTaskTestCase.setIdTestCase(tcId);
			save(assoTaTaskTestCase);
		}
		return 0;
	}
}
