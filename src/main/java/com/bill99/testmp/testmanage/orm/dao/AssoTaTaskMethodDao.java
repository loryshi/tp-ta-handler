package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskMethod;

public interface AssoTaTaskMethodDao extends BaseDao<AssoTaTaskMethod, String> {

	AssoTaTaskMethod getById(Long idTaTask, Long idSvnMethod);

}
