package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.ibatis.dao.JiraIbatisDao;
import com.bill99.testmp.testmanage.orm.manager.JiraMng;

public class JiraMngImpl implements JiraMng {

	private JiraIbatisDao jiraIbatisDao;

	public void setJiraIbatisDao(JiraIbatisDao jiraIbatisDao) {
		this.jiraIbatisDao = jiraIbatisDao;
	}

	public List<String> getEmail4Role(Long[] roleIds, Long[] teemIds) {
		return jiraIbatisDao.getEmail4Role(roleIds, teemIds);
	}

	public List<String> getEmail4RoleIssue(Long[] roleIssueIds, Long issueId) {
		return jiraIbatisDao.getEmail4RoleIssue(roleIssueIds, issueId);
	}
}
