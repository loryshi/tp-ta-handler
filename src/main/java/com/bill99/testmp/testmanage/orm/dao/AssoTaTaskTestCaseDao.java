package com.bill99.testmp.testmanage.orm.dao;

import com.bill99.seashell.orm.BaseDao;
import com.bill99.testmp.testmanage.orm.entity.AssoTaTaskTestCase;

public interface AssoTaTaskTestCaseDao extends BaseDao<AssoTaTaskTestCase, String> {

	int bath4insert(Long[] tcIds, Long idTaTask);

	int insert4bath(Long[] tcIds, Long idTaTask);

}
