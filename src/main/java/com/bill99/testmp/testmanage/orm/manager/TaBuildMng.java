package com.bill99.testmp.testmanage.orm.manager;

import java.util.List;

import com.bill99.testmp.testmanage.orm.entity.TaBuild;

public interface TaBuildMng {

	public TaBuild query4rp(String jobName, String buildId, String buildNumber);

	public TaBuild getTaBuild(Long idTaTask, Long testPlanId);

	public TaBuild save(TaBuild taBuild);

	public void update(TaBuild taBuild);

	public List<TaBuild> queryByJob(String jobName);

	public TaBuild merge(TaBuild taBuild);

	public Long getIdTaProject(Long idTaTask, String jobName);

}
