package com.bill99.testmp.testmanage.orm.manager.impl;

import java.util.List;

import com.bill99.testmp.testmanage.orm.dao.TaBuildDao;
import com.bill99.testmp.testmanage.orm.entity.TaBuild;
import com.bill99.testmp.testmanage.orm.manager.TaBuildMng;

public class TaBuildMngImpl implements TaBuildMng {

	private TaBuildDao taBuildDao;

	public void setTaBuildDao(TaBuildDao taBuildDao) {
		this.taBuildDao = taBuildDao;
	}

	public TaBuild query4rp(String jobName, String buildId, String buildNumber) {
		return taBuildDao.query4rp(jobName, buildId, buildNumber);
	}

	public TaBuild save(TaBuild taBuild) {
		return taBuildDao.save(taBuild);
	}

	public void update(TaBuild taBuild) {
		taBuildDao.update(taBuild);
	}

	public List<TaBuild> queryByJob(String jobName) {
		return taBuildDao.queryByJob(jobName);
	}

	public TaBuild merge(TaBuild taBuild) {
		return taBuildDao.merge(taBuild);
	}

	public TaBuild getTaBuild(Long idTaTask, Long testPlanId) {
		return taBuildDao.getTaBuild(idTaTask, testPlanId);
	}

	public Long getIdTaProject(Long idTaTask, String jobName) {
		return taBuildDao.getIdTaProject(idTaTask, jobName);
	}

}
