package com.bill99.testmp.testmanage.domain;

import com.bill99.testmp.ta.mdp.api.common.TaTaskRequestBo;
import com.bill99.testmp.ta.mdp.api.common.TaTaskResponesBo;

public interface TaTaskTiggerService {

	public TaTaskResponesBo triggerTaTask(TaTaskRequestBo taTaskRequestBo);

	public void checkTaskTigger();

	public void tiggerTask(Long idTaTask, TaTaskRequestBo taTaskRequestBo);

	public void autoTiggerTask();

	public void autoTiggerTask4Regression();

}
