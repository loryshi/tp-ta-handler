package com.bill99.testmp.testmanage.domain;

public interface TaTaskNotifier {

	public void taTaskEmailReport(String taTaskId, Long planStepId, Long testProjectId);

}
