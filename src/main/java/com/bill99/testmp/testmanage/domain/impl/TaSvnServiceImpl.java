package com.bill99.testmp.testmanage.domain.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.bill99.riaframework.common.dto.TaSvnMethodDto;
import com.bill99.riaframework.common.helper.Bill99Logger;
import com.bill99.testmp.framework.helper.SvnTaListHelper;
import com.bill99.testmp.testmanage.domain.TaSvnService;
import com.bill99.testmp.testmanage.orm.entity.TaProject;
import com.bill99.testmp.testmanage.orm.entity.TaSvnClass;
import com.bill99.testmp.testmanage.orm.entity.TaSvnMethod;
import com.bill99.testmp.testmanage.orm.manager.TaProjectMng;
import com.bill99.testmp.testmanage.orm.manager.TaSvnClassMng;

public class TaSvnServiceImpl implements TaSvnService {
	private Bill99Logger logger = Bill99Logger.getLogger(this.getClass());

	private TaProjectMng taProjectMng;
	private TaSvnClassMng taSvnClassMng;

	private String svnUserName;
	private String svnPassword;

	public void initTaSvnPath() {
		List<TaProject> taProjects = taProjectMng.find4List();

		if (taProjects != null) {
			TaSvnClass taSvnClass = null;
			for (TaProject taProject : taProjects) {
				if (StringUtils.hasLength(taProject.getTaSvnUrl())) {

					List<TaSvnClass> svnTaPaths = new ArrayList<TaSvnClass>();
					Map<String, List<TaSvnMethodDto>> taSvnPathMap = SvnTaListHelper.parseSvn(taProject.getTaSvnUrl().trim(), svnUserName, svnPassword);
					for (Entry<String, List<TaSvnMethodDto>> entry : taSvnPathMap.entrySet()) {
						taSvnClass = new TaSvnClass();
						taSvnClass.setIdTaProject(taProject.getIdTaProject());
						taSvnClass.setFilePath(entry.getKey());//文件地址

						String[] fileNameTmp = entry.getKey().split("/");
						taSvnClass.setClassName(fileNameTmp[fileNameTmp.length - 1].replace(".java", ""));//类名
						List<TaSvnMethod> taSvnMethods = new ArrayList<TaSvnMethod>();
						TaSvnMethod taSvnMethod = null;
						for (TaSvnMethodDto taSvnMethodDto : entry.getValue()) {
							taSvnMethod = new TaSvnMethod();
							taSvnMethod.setMethodName(taSvnMethodDto.getMethodName());
							taSvnMethod.setMemo(taSvnMethodDto.getDescription());//描述 需要统一编码后开启,否则乱码
							taSvnClass.setPackageName(taSvnMethodDto.getPackageName());
							taSvnMethod.setDependsOnMethods(taSvnMethodDto.getDependsOnMethods());
							taSvnMethods.add(taSvnMethod);
						}
						taSvnClass.setTaSvnMethods(taSvnMethods);
						if (taSvnMethods.size() != 0) {
							svnTaPaths.add(taSvnClass);
						}
					}
					if (svnTaPaths.size() > 0) {
						int rowsClass = taSvnClassMng.bath4merge(svnTaPaths, taProject.getIdTaProject());
						int rowsMethod = taSvnClassMng.bath4mergeMethod(svnTaPaths, taProject.getIdTaProject());
						logger.debug(taProject.getIdTaProject() + " @ " + taProject.getTaSvnUrl() + " delete class rows = " + rowsClass + " & method rows = " + rowsMethod);
					}
				}
			}
		}

	}

	public void setSvnUserName(String svnUserName) {
		this.svnUserName = svnUserName;
	}

	public void setSvnPassword(String svnPassword) {
		this.svnPassword = svnPassword;
	}

	public void setTaSvnClassMng(TaSvnClassMng taSvnClassMng) {
		this.taSvnClassMng = taSvnClassMng;
	}

	public void setTaProjectMng(TaProjectMng taProjectMng) {
		this.taProjectMng = taProjectMng;
	}

}
